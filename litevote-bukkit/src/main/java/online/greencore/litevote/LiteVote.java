package online.greencore.litevote;

import net.milkbowl.vault.economy.Economy;
import online.greencore.litevote.io.Storage;
import online.greencore.litevote.io.YamlUserFileStorage;
import online.greencore.litevote.io.jdbc.JdbcStorage;
import online.greencore.litevote.model.MilestoneHandler;
import online.greencore.litevote.model.RewardHandler;
import online.greencore.litevote.model.UserHandler;
import online.greencore.litevote.model.VoteManager;
import online.greencore.litevote.model.commands.VoteCommands;
import online.greencore.litevote.model.listeners.PlayerListeners;
import online.greencore.litevote.model.listeners.VoteListeners;
import online.greencore.litevote.util.IOUtils;
import online.greencore.litevote.util.LiteVotePlaceholders;
import online.greencore.litevote.util.MessagesConfiguration;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.SimpleFormatter;

public class LiteVote extends JavaPlugin {

    private static final String VERSION = "1.4-DEV";
    private final File config = new File(getDataFolder(), "config.properties");
    private Properties properties = new Properties();
    private Storage storage;
    private MessagesConfiguration messageConfig;
    private VoteManager voteManager;
    private UserHandler userHandler;
    private RewardHandler rewardHandler;
    private MilestoneHandler milestoneHandler;

    private LiteVotePlaceholders placeholders;
    private Economy economy;

    public static String getUUID(String name) {
        Player player = Bukkit.getPlayer(name);
        if (player != null) {
            return player.getUniqueId().toString();
        } else {
            return Bukkit.getOfflinePlayer(name).getUniqueId().toString();
        }
    }

    @Override
    public void onEnable() {
        try {
            File file = new File(getDataFolder() + File.separator + "logs", "debug.log");
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            FileHandler handler = new FileHandler(file.getAbsolutePath());
            handler.setLevel(Level.ALL);
            getLogger().addHandler(handler);
            SimpleFormatter formatter = new SimpleFormatter();
            handler.setFormatter(formatter);
        } catch (IOException e) {
            getLogger().severe("Error setting up logger: " + e.getCause());
        }

        if (Bukkit.getPluginManager().getPlugin("Votifier") == null) {
            getLogger().severe(String.format("[%s] - Disabled due to no Votifier dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        setupResources();
        try {
            properties.load(new FileInputStream(config));
            messageConfig = new MessagesConfiguration(this);
            userHandler = new UserHandler(this);

            rewardHandler = new RewardHandler(this);
            voteManager = new VoteManager(this);
            milestoneHandler = new MilestoneHandler(this);

            placeholders = new LiteVotePlaceholders(this);
            placeholders.hook();

            storage = createUserStorage();
            storage.init();
        } catch (Exception e) {
            e.printStackTrace();
            getServer().getPluginManager().disablePlugin(this);
        }

        getServer().getPluginManager().registerEvents(new PlayerListeners(this), this);
        getServer().getPluginManager().registerEvents(new VoteListeners(this), this);
        getCommand("vote").setExecutor(new VoteCommands(this));
        getLogger().info("Loaded Version: " + VERSION);
    }

    @Override
    public void onDisable() {
        storage = null;
        properties = null;
        rewardHandler = null;
        voteManager = null;
        userHandler = null;
        milestoneHandler = null;
    }

    public boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        economy = rsp.getProvider();
        return economy != null;
    }

    private Storage createUserStorage() throws Exception {
        String type = properties.getProperty("storage").toLowerCase();
        getLogger().config("Using storage: " + type);
        switch (type) {
            case "mysql":
                return new JdbcStorage(this);
            default:
                getLogger().info("Using default storage: YAML");
            case "yml":
            case "yaml":
                return new YamlUserFileStorage(this);
        }
    }

    private void setupResources() {
        if (!getDataFolder().exists()) {
            getDataFolder().mkdirs();
        }
        if (!config.exists()) {
            IOUtils.loadResource(this, "config.properties", ".");
        }

        IOUtils.checkResource(this, "messages.yml", ".");
        IOUtils.checkResource(this, "milestones.yml", ".");
        IOUtils.checkResource(this, "PMC.yml", File.separator + "sites");
        IOUtils.checkResource(this, "BasicReward.yml", File.separator + "rewards");
        IOUtils.checkResource(this, "AdvancedReward.yml", File.separator + "rewards");
        getLogger().config("Configured plugin resources");
    }

    public Properties getProperties() {
        return properties;
    }

    public Storage getStorage() {
        return storage;
    }

    public MessagesConfiguration getMessages() {
        return messageConfig;
    }

    public RewardHandler getRewardHandler() {
        return rewardHandler;
    }

    public UserHandler getUserHandler() {
        return userHandler;
    }

    public VoteManager getVoteManager() {
        return voteManager;
    }

    public MilestoneHandler getMilestoneHandler() {
        return milestoneHandler;
    }

    public LiteVotePlaceholders getPlaceholders() {
        return placeholders;
    }

    public Economy getEconomy() {
        return economy;
    }
}
