package online.greencore.litevote.io;

import online.greencore.litevote.model.User;
import online.greencore.litevote.model.UserHandler;
import org.bukkit.Bukkit;

public abstract class Storage {

    public void init() {
        Bukkit.getWorlds().forEach(world -> world.getPlayers().forEach(player -> {
            User user = load(new User(player.getUniqueId().toString()));
            UserHandler.getCache().putIfAbsent(user.getUUID(), user);
        }));
    }

    public abstract Result save(User user);

    public abstract User load(User user);

    public enum Result {
        SUCCESS,
        NOT_FOUND,
        FAILED
    }
}
