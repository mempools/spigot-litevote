package online.greencore.litevote.io;

import online.greencore.litevote.LiteVote;
import online.greencore.litevote.model.User;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class YamlUserFileStorage extends Storage {

    private final LiteVote plugin;
    private final File dataFile;
    private File userFile;
    private FileConfiguration configuration;

    public YamlUserFileStorage(LiteVote plugin) {
        this.plugin = plugin;
        this.dataFile = new File(plugin.getDataFolder() + File.separator + "storage");
        if (!dataFile.exists()) {
            dataFile.mkdirs();
        }
    }

    @Override
    public Result save(User user) {
        userFile = new File(dataFile, File.separator + user.getUUID() + ".yml");
        if (!userFile.exists()) {
            return Result.NOT_FOUND;
        }
        configuration = YamlConfiguration.loadConfiguration(userFile);
        configuration.set("uuid", user.getUUID());
        configuration.set("points", user.points);
        configuration.set("milestone", user.milestone);
        configuration.createSection("claimedMilestones", user.claimedMilestones);
        configuration.createSection("voteTotals", user.totals);
        configuration.set("offlineVotes", user.offlineVotes);
        try {
            configuration.save(userFile);
        } catch (IOException e) {
            plugin.getLogger().severe("Error Saving User to YML file:\n " + e.getMessage());
            return Result.FAILED;
        }
        return Result.SUCCESS;
    }

    @Override
    public User load(User user) {
        userFile = new File(dataFile, user.getUUID() + ".yml");
        if (!userFile.exists() || userFile.length() == 0) {
            Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
                try {
                    YamlConfiguration.loadConfiguration(userFile).save(userFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            return user;
        }
        configuration = YamlConfiguration.loadConfiguration(userFile);
        user.points = configuration.getInt("points");
        user.milestone = configuration.getInt("milestone");
        ConfigurationSection claimedMilestones = configuration.getConfigurationSection("claimedMilestones");
        claimedMilestones.getKeys(false).forEach(milestone -> user.claimedMilestones.put(Integer.parseInt(milestone), claimedMilestones.getBoolean(milestone)));
        ConfigurationSection totals = configuration.getConfigurationSection("voteTotals");
        totals.getKeys(false).forEach(serviceName -> user.totals.put(serviceName, totals.getInt(serviceName)));
        user.offlineVotes = configuration.getStringList("offlineVotes");
        return user;
    }
}
