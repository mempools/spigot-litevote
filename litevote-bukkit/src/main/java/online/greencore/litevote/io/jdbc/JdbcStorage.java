package online.greencore.litevote.io.jdbc;

import online.greencore.litevote.LiteVote;
import online.greencore.litevote.io.Storage;
import online.greencore.litevote.model.User;
import online.greencore.litevote.util.Database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;

public class JdbcStorage extends Storage {

    private final LiteVote plugin;
    private final Connection connection;
    private final JdbcUserStorage jdbcUserStorage;
    private final String prefix;
    private final PreparedStatement registerStatement;

    public JdbcStorage(LiteVote plugin) throws SQLException {
        this.plugin = plugin;
        this.connection = new Database(plugin).getConnection();
        this.prefix = plugin.getProperties().getProperty("mysql_prefix");
        this.jdbcUserStorage = new JdbcUserStorage(prefix, connection);
        this.registerStatement = connection.prepareStatement("INSERT INTO " + prefix + "votes (uuid, points, milestones) VALUES (?, ?, ?);");
    }

    private void populate() {
        String voteTable = "CREATE TABLE IF NOT EXISTS `" + prefix + "votes` (\n"
                + "  `id` int(11) NOT NULL AUTO_INCREMENT,\n"
                + "  `uuid` varchar(36) DEFAULT NULL,\n"
                + "  `points` tinyint(2) NOT NULL DEFAULT '0',\n"
                + "  `milestones` tinyint(2) NOT NULL DEFAULT '0',\n"
                + "  `offlineVotes` varchar(255) NOT NULL DEFAULT '',\n"
                + "  PRIMARY KEY (`id`),\n"
                + "  UNIQUE KEY `uuid` (`uuid`)\n"
                + ") ENGINE=InnoDB DEFAULT CHARSET=utf8";

        String totalsTable = "CREATE TABLE IF NOT EXISTS `" + prefix + "totals` (\n"
                + "  `id` int(11) NOT NULL AUTO_INCREMENT,\n"
                + "  `uuid` varchar(36) DEFAULT NULL,\n"
                + "  `service` varchar(15) NOT NULL,\n"
                + "  `points` tinyint(4) NOT NULL DEFAULT '0',\n"
                + "  PRIMARY KEY (`id`),\n"
                + "  UNIQUE KEY `user_totals` (`uuid`,`service`)\n"
                + ") ENGINE=InnoDB DEFAULT CHARSET=utf8 ";

        String claimedMilestones = "CREATE TABLE IF NOT EXISTS `" + prefix + "claimed_milestones` (\n"
                + "  `id` int(11) NOT NULL AUTO_INCREMENT,\n"
                + "  `uuid` varchar(36) DEFAULT NULL,\n"
                + "  `milestone` tinyint(4) NOT NULL,\n"
                + "  `voted` tinyint(1) NOT NULL DEFAULT '0',\n"
                + "  PRIMARY KEY (`id`),\n"
                + "  UNIQUE KEY `user_claimed` (`uuid`,`service`)\n"
                + ") ENGINE=InnoDB DEFAULT CHARSET=utf8 ";
        try {
            Statement statement = connection.createStatement();
            statement.addBatch(voteTable);
            statement.addBatch(totalsTable);
            statement.addBatch(claimedMilestones);
            statement.executeBatch();
            connection.commit();
        } catch (SQLException e) {
            plugin.getLogger().severe("Error creating tables: " + prefix + " ");
            e.printStackTrace();
        }
    }

    @Override
    public void init() {
        populate();
    }

    @Override
    public Result save(User user) {
        try {
            jdbcUserStorage.save(user);
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ignored) {
            }
            return Result.FAILED;
        }
        return Result.SUCCESS;
    }

    @Override
    public User load(User user) {
        Optional<User> load = Optional.ofNullable(jdbcUserStorage.load(user));
        if (!load.isPresent()) register(user);
        return load.orElse(user);
    }

    private boolean register(User user) {
        try (Connection con = connection) {
            registerStatement.setString(1, user.getUUID());
            registerStatement.setInt(2, user.points);
            registerStatement.setInt(3, user.milestone);
            registerStatement.execute();
            con.commit();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

}
