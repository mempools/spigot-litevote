package online.greencore.litevote.io.jdbc;

import online.greencore.litevote.io.Storage;
import online.greencore.litevote.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class JdbcUserStorage extends Storage {

    private final PreparedStatement voteStatement, totalStatement, loadVotes, loadTotals, loadClaimed, saveClaimed;

    JdbcUserStorage(String prefix, Connection connection) throws SQLException {
        this.voteStatement = connection.prepareStatement("REPLACE INTO " + prefix + "votes (uuid, points, milestones, offlineVotes) VALUES (?, ?, ?, ?)");
        this.totalStatement = connection.prepareStatement("INSERT INTO " + prefix + "totals (uuid, service, points) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE points = VALUES(points);");
        this.loadVotes = connection.prepareStatement("SELECT * FROM " + prefix + "votes WHERE uuid = ?");
        this.loadTotals = connection.prepareStatement("SELECT * FROM " + prefix + "totals WHERE uuid = ?");
        this.loadClaimed = connection.prepareStatement("SELECT * FROM " + prefix + "claimed_milestones WHERE uuid = ?");
        this.saveClaimed = connection.prepareStatement("INSERT INTO " + prefix + "claimed_milestones (uuid, milestone, voted) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE voted = VALUES(voted);");

    }

    @Override
    public Result save(User user) {
        try {
            voteStatement.setString(1, user.getUUID());
            voteStatement.setInt(2, user.points);
            voteStatement.setInt(3, user.milestone);
            StringBuilder sb = new StringBuilder();
            user.offlineVotes.forEach(offline -> sb.append(offline).append(','));
            voteStatement.setString(4, sb.toString());
            voteStatement.execute();
            user.totals.forEach((service, points) -> {
                try {
                    totalStatement.setString(1, user.getUUID());
                    totalStatement.setString(2, service);
                    totalStatement.setInt(3, points);
                    totalStatement.addBatch();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
            totalStatement.executeBatch();

            user.claimedMilestones.forEach((milestone, voted) -> {
                try {
                    saveClaimed.setString(1, user.getUUID());
                    saveClaimed.setInt(2, milestone);
                    saveClaimed.setBoolean(3, voted);
                    saveClaimed.addBatch();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
            saveClaimed.executeBatch();

            return Result.SUCCESS;
        } catch (SQLException e) {
            e.printStackTrace();
            return Result.FAILED;
        }
    }

    @Override
    public User load(User user) {
        try {
            loadVotes.setString(1, user.getUUID());
            ResultSet set = loadVotes.executeQuery();
            if (set.first()) {
                int points = set.getInt("points");
                int milestones = set.getInt("milestone");
                user.points = points;
                user.milestone = milestones;
                user.offlineVotes = new ArrayList<>(Arrays.asList(set.getString("offlineVotes").split(",")));

                loadTotals.setString(1, user.getUUID());
                set = loadTotals.executeQuery();
                Map<String, Integer> totalMap = new HashMap<>();
                while (set.next()) {
                    totalMap.put(set.getString("service"), set.getInt("points"));
                }
                user.totals = totalMap;

                loadClaimed.setString(1, user.getUUID());
                set = loadClaimed.executeQuery();
                Map<Integer, Boolean> claimedMap = new HashMap<>();
                while (set.next()) {
                    claimedMap.put(set.getInt("milestone"), set.getBoolean("voted"));
                }
                user.claimedMilestones = claimedMap;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }
}
