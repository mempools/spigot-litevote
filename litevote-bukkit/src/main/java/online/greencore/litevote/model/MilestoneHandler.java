package online.greencore.litevote.model;

import online.greencore.litevote.LiteVote;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MilestoneHandler {

    private final RewardHandler rewardHandler;
    private final Map<Integer, List<Reward>> milestones = new ConcurrentHashMap<>();

    public MilestoneHandler(LiteVote plugin) {
        this.rewardHandler = plugin.getRewardHandler();

        File milestoneFile = new File(plugin.getDataFolder() + File.separator + "milestones.yml");
        if (!milestoneFile.exists()) {
            milestoneFile.mkdir();
            return;
        }

        YamlConfiguration parser = YamlConfiguration.loadConfiguration(milestoneFile);
        parser.getKeys(false).forEach(number -> {
            int milestone = Integer.parseInt(number);
            List<Reward> rewardList = new ArrayList<>();
            List<String> rewards = parser.getStringList(number + ".rewards");
            rewards.forEach(reward -> {
                if (rewardHandler.getRewards().containsKey(reward)) {
                    Reward newReward = rewardHandler.getRewards().get(reward);
                    rewardList.add(newReward);
                }
            });
            milestones.put(milestone, rewardList);
        });
        plugin.getLogger().info("Loaded " + milestones.size() + " Milestone rewards");
    }

    public void addMilestone(User user, int amount) {
        user.milestone += amount;
        if (milestones.containsKey(user.milestone)) {
            user.claimedMilestones.putIfAbsent(user.milestone, false);
        }
    }

    public void claimMilestoneRewards(Player player, User user) {
        user.claimedMilestones.forEach((milestone, claimed) -> {
            if (user.milestone >= milestone && !claimed) {
                if (!milestones.containsKey(milestone)) {
                    return;
                }
                List<Reward> rewardList = milestones.get(milestone);
                rewardHandler.applyRewards(player, rewardList);
                if (user.claimedMilestones.containsKey(milestone)) {
                    user.claimedMilestones.replace(milestone, true);
                } else {
                    user.claimedMilestones.putIfAbsent(milestone, true);
                }
            }
        });
    }

}
