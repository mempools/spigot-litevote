package online.greencore.litevote.model;

import org.bukkit.Sound;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Reward {

    private final String name;
    public List<String> console;
    public List<String> player;
    public SoundData playSound;
    public List<PotionEffect> potionEffects;
    public List<String> giveItems;
    double money = 0;

    public Reward(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    private SoundData create(Sound sound, int vol, int pitch) {
        return new SoundData(sound, vol, pitch);
    }

    void parseCommands(ConfigurationSection parser) {
        console = parser.getStringList("console");
        player = parser.getStringList("player");
    }

    void parseSound(ConfigurationSection parser) {
        Optional<ConfigurationSection> oSound = Optional.ofNullable(parser);
        oSound.ifPresent(section -> {
            Sound sound = Sound.valueOf(section.getString("sound"));
            int vol = section.getInt("vol");
            int pitch = section.getInt("pitch");
            playSound = create(sound, vol, pitch);
        });
    }

    void parsePotionEffects(ConfigurationSection parser) {
        Optional<ConfigurationSection> sectionPotions = Optional.ofNullable(parser);
        sectionPotions.ifPresent(potion -> {
            List<PotionEffect> list = new ArrayList<>();
            potion.getKeys(false).forEach(name -> {
                PotionEffectType effect = PotionEffectType.getByName(name);
                int duration = potion.getInt(name + ".duration");
                int amp = potion.getInt(name + ".amp");
                list.add(new PotionEffect(effect, duration * 20, amp));
            });
            potionEffects = list;
        });
    }

    void parseItems(ConfigurationSection parser) {
        List<String> commands = new ArrayList<>();
        Optional<ConfigurationSection> sectionItems = Optional.ofNullable(parser);
        sectionItems.ifPresent(section -> section.getKeys(false).forEach(itemName -> {
            String player = Optional.ofNullable(section.getString(itemName + ".parameter")).orElse("%litevote_player%");
            int itemAmount = Optional.ofNullable(section.getInt(itemName + ".amount")).orElse(1);
            int power = Optional.ofNullable(section.getInt(itemName + ".damage")).orElse(0);
            StringBuilder sb = new StringBuilder();
            sb.append("give").append(' ').append(player).append(' ').append(itemName).append(' ');
            sb.append(itemAmount).append(' ').append(power).append(' ');
            sb.append('{');
            Optional<Integer> repairTable = Optional.ofNullable(section.getInt(itemName + ".repairCost"));
            repairTable.ifPresent(integer -> sb.append("repairCost:").append('"').append(integer).append('"').append(','));

            Optional<Boolean> unbreakable = Optional.ofNullable(section.getBoolean(itemName + ".unbreakable"));
            unbreakable.ifPresent(b -> sb.append("unbreakable:").append(b ? 1 : 0).append(','));

            Optional<Boolean> hideEnchants = Optional.ofNullable(section.getBoolean(itemName + ".hideEnchants"));
            hideEnchants.ifPresent(b -> sb.append("HideFlags:").append(b ? 1 : 0).append(','));

            Optional<ConfigurationSection> lore = Optional.ofNullable(section.getConfigurationSection(itemName + ".lore"));
            lore.ifPresent(loreSec -> {
                String name = loreSec.getString("name");
                List<String> description = loreSec.getStringList("description");
                sb.append("display:").append('{');
                sb.append("Name:").append('"').append(name).append('"');

                sb.append(',');

                sb.append("Lore:").append('[');
                description.forEach(desc -> {
                    sb.append('"').append(desc).append('"');
                    sb.append(',');
                });
                sb.deleteCharAt(sb.length() - 1);
                sb.append(']');
                sb.append("}");
            });

            Optional<ConfigurationSection> enchantDatas = Optional.ofNullable(section.getConfigurationSection(itemName + ".enchantments"));
            enchantDatas.ifPresent(elist -> {
                sb.append(",ench:[");
                elist.getKeys(false).forEach(enchantment -> {
                    Enchantment enchant = Enchantment.getByName(enchantment);
                    int lvl = elist.getInt(enchantment + ".level");
                    sb.append('{').append("id:").append(enchant.hashCode()).append(',');
                    sb.append("lvl:").append(lvl).append('}');
                    sb.append(',');
                });
                sb.deleteCharAt(sb.length() - 1);
                sb.append(']');
            });

            Optional<ConfigurationSection> attributes = Optional.ofNullable(section.getConfigurationSection(itemName + ".attributes"));
            attributes.ifPresent(attr -> {
                enchantDatas.ifPresent(e -> sb.append(','));
                sb.append("AttributeModifiers:").append('[');
                attr.getKeys(false).forEach(add -> {
                    Attribute attribute = Attribute.valueOf(add);
                    double amt = attr.getDouble(add + ".amount");
                    AttributeModifier.Operation operation = AttributeModifier.Operation.valueOf(attr.getString(add + ".operation"));
                    AttributeModifier am = new AttributeModifier(attribute.name, amt, operation);

                    sb.append('{');
                    sb.append("AttributeName:").append('"').append(am.getName()).append('"').append(',');
                    sb.append("Name:").append('"').append(am.getName()).append('"').append(',');
                    sb.append("Amount:").append(am.getAmount()).append(',');
                    sb.append("Operation:").append(am.getOperation().ordinal()).append(',');
                    sb.append("UUIDMost:").append((int) am.getUniqueId().getMostSignificantBits()).append(',');
                    sb.append("UUIDLeast:").append((int) am.getUniqueId().getLeastSignificantBits());
                    sb.append('}').append(',');
                });
                sb.deleteCharAt(sb.length() - 1);
                sb.append(']');
            });
            sb.append('}');
            commands.add(sb.toString());
        }));
        giveItems = commands;
    }

    private enum Attribute {
        MAX_HEALTH("generic.maxHealth"),
        FOLLOW_RANGE("generic.followRange"),
        KNOCKBACK_RESISTANCE("generic.knockbackResistance"),
        MOVEMENT_SPEED("generic.movementSpeed"),
        ATTACK_DAMAGE("generic.attackDamage"),
        ARMOR("generic.armor"),
        ARMOR_TOUGHNESS("generic.armorToughness");

        public final String name;

        Attribute(String name) {
            this.name = name;
        }
    }

    public class SoundData {

        Sound sound;
        int volume;
        int pitch;

        SoundData(Sound sound, int volume, int pitch) {
            this.sound = sound;
            this.volume = volume;
            this.pitch = pitch;
        }
    }
}
