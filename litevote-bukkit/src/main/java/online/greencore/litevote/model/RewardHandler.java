package online.greencore.litevote.model;

import online.greencore.litevote.LiteVote;
import online.greencore.litevote.util.PlaceholderFilter;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

public class RewardHandler {

    private final LiteVote plugin;
    private final Map<String, Reward> rewards = new HashMap<>();

    public RewardHandler(LiteVote plugin) {
        this.plugin = plugin;
        File rewardsDir = new File(plugin.getDataFolder() + File.separator + "rewards");
        if (!rewardsDir.exists()) {
            rewardsDir.mkdir();
        }
        try (Stream<Path> paths = Files.walk(Paths.get(rewardsDir.getPath()))) {
            paths.filter(Files::isRegularFile).filter(f -> f.toFile().getName().contains(".yml")).forEach(filePath -> {
                FileConfiguration parser = YamlConfiguration.loadConfiguration(filePath.toFile());
                Reward reward = new Reward(filePath.toFile().getName().replace(".yml", ""));
                reward.money = parser.getDouble("money");
                reward.parseCommands(parser.getConfigurationSection("commands"));
                reward.parseSound(parser.getConfigurationSection("play-sound"));
                reward.parsePotionEffects(parser.getConfigurationSection("potion-effects"));
                reward.parseItems(parser.getConfigurationSection("items"));
                rewards.put(reward.getName(), reward);
            });
        } catch (IOException e) {
            plugin.getLogger().severe("Error Loading Reward Files: " + e.getCause());
            e.printStackTrace();
        }
    }

    public void applyRewards(Player player, List<Reward> rewardSet) {
        rewardSet.forEach(reward -> {
            ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
            Optional.of(reward.money).filter(m -> plugin.setupEconomy()).ifPresent(money -> plugin.getEconomy().depositPlayer(player, money));
            Optional.ofNullable(reward.console).ifPresent(cCmd -> cCmd.forEach(command -> Bukkit.dispatchCommand(console, PlaceholderFilter.filter(player, command))));
            Optional.ofNullable(reward.player).ifPresent(pCmd -> pCmd.forEach(command -> Bukkit.dispatchCommand(player, PlaceholderFilter.filter(player, command))));
            Optional.ofNullable(reward.playSound).ifPresent(sound -> player.playSound(player.getLocation(), sound.sound, sound.volume, sound.pitch));
            Optional.ofNullable(reward.potionEffects).ifPresent(player::addPotionEffects);
            Optional.ofNullable(reward.giveItems).ifPresent(giveItems -> giveItems.forEach(command -> Bukkit.dispatchCommand(console, PlaceholderFilter.filter(player, command))));
        });
    }

    Map<String, Reward> getRewards() {
        return rewards;
    }
}
