package online.greencore.litevote.model;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class User {

    private final String uuid;
    public boolean online;
    public int points;
    public int milestone;
    public Map<String, Integer> totals = new ConcurrentHashMap<>();
    public Map<Integer, Boolean> claimedMilestones = new ConcurrentHashMap<>();
    public List<String> offlineVotes = new ArrayList<>();
    private int totalCount = 0;

    public User(UUID uuid) {
        this.uuid = uuid.toString();
    }

    public User(String uuid) {
        this.uuid = uuid;
    }

    public String getUUID() {
        return uuid;
    }

    public void addPoints(int amount) {
        points = points + amount;
    }

    public void addTotals(String serviceName, int amount) {
        if (!totals.containsKey(serviceName)) {
            totals.putIfAbsent(serviceName, amount);
        } else {
            totals.replace(serviceName, totals.get(serviceName) + amount);
        }
    }

    public int getTotal() {
        if (totals != null) {
            totals.forEach((s, integer) -> totalCount += integer);
        } else {
            totalCount = 0;
        }
        return totalCount;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof String) {
            String uuid = (String) object;
            //if the object is a string, let's compare the UUID's
            return Objects.equals(this.uuid, uuid);
        } else if (object instanceof User) {
            User user = (User) object;
            //the object is an instance of User.class
            return Objects.equals(uuid, user.uuid);
        }
        Optional<Object> oUser = Optional.ofNullable(object);
        return this == oUser.orElse(false);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid.hashCode(), points, milestone, totals.hashCode());
    }

    @Override
    public String toString() {
        return "User{" +
                "uuid='" + uuid + '\'' +
                ", online=" + online +
                ", points=" + points +
                ", milestone=" + milestone +
                ", totals=" + totals +
                ", claimedMilestones=" + claimedMilestones +
                ", offlineVotes=" + offlineVotes.size() +
                ", totalCount=" + totalCount +
                '}';
    }
}
