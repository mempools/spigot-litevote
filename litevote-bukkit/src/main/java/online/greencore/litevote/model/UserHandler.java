package online.greencore.litevote.model;

import online.greencore.litevote.LiteVote;
import online.greencore.litevote.util.MessagesConfiguration;
import online.greencore.litevote.util.PlaceholderFilter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class UserHandler {

    private static final Map<String, User> cache = new ConcurrentHashMap<>();
    private final LiteVote plugin;
    private final MessagesConfiguration messagesConf;

    public UserHandler(LiteVote plugin) {
        this.plugin = plugin;
        this.messagesConf = plugin.getMessages();
    }

    public static Map<String, User> getCache() {
        return cache;
    }

    public User getUser(String uuid) {
        if (!cache.containsKey(uuid)) cache.putIfAbsent(uuid, new User(uuid));
        else return cache.get(uuid);
        return plugin.getStorage().load(new User(uuid));
    }

    public void save(User user) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> plugin.getStorage().save(Optional.ofNullable(cache.get(user.getUUID())).orElse(user)));
    }

    public boolean hasPendingClaims(Player player, User user, boolean claim) {
        if (user == null) return false;
        if (user.offlineVotes.size() >= 1) {
            if (claim) {
                user.offlineVotes.forEach(site -> plugin.getRewardHandler().applyRewards(player, plugin.getVoteManager().getService(site).reward));
                user.offlineVotes.clear();
            } else {
                messagesConf.unclaimedRewards.forEach(message -> PlaceholderFilter.sendMessage(player, message));
            }
        } else if (user.claimedMilestones.containsValue(false)) {
            if (claim) {
                plugin.getMilestoneHandler().claimMilestoneRewards(player, user);
            } else {
                messagesConf.unclaimedMilestones.forEach(message -> PlaceholderFilter.sendMessage(player, message));
            }
        } else {
            if (claim)
                plugin.getMessages().claimNothing.forEach(message -> PlaceholderFilter.sendMessage(player, message));
            return false;
        }
        return true;
    }
}
