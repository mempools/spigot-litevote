package online.greencore.litevote.model;

import online.greencore.litevote.LiteVote;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

public class VoteManager {

    private final Map<String, VoteService> services = new ConcurrentHashMap<>();

    public VoteManager(LiteVote plugin) {
        File sitesDir = new File(plugin.getDataFolder() + File.separator + "sites");
        if (!sitesDir.exists()) {
            sitesDir.mkdir();
        }
        try (Stream<Path> paths = Files.walk(Paths.get(sitesDir.getPath()))) {
            paths.filter(Files::isRegularFile).filter(f -> f.toFile().getName().contains(".yml")).forEach(filePath -> {
                FileConfiguration parser = YamlConfiguration.loadConfiguration(filePath.toFile());
                if (parser == null) {
                    return;
                }
                VoteService vote = new VoteService(parser.getString("service"));
                vote.url = parser.getString("url");
                vote.delay = Optional.of(parser.getInt("delay")).orElse(24);
                List<String> rewards = Optional.of(parser.getStringList("rewards")).orElse(Collections.emptyList());
                List<Reward> rewardList = new ArrayList<>();
                if (!rewards.isEmpty()) {
                    rewards.forEach(reward -> {
                        if (plugin.getRewardHandler().getRewards().containsKey(reward)) {
                            rewardList.add(plugin.getRewardHandler().getRewards().get(reward));
                        } else {
                            plugin.getLogger().warning("Missing reward file: " + reward);
                        }
                    });
                } else {
                    plugin.getLogger().warning("No rewards defined for " + vote.getService() + "!");
                }
                vote.reward = rewardList;
                services.put(vote.getService(), vote);
            });
            plugin.getLogger().config("Loaded " + services.size() + " vote site configurations ");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Map<String, VoteService> getServices() {
        return services;
    }

    public VoteService getService(String name) {
        return services.get(name);
    }

}
