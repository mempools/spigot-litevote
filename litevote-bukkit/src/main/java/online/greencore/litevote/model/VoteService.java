package online.greencore.litevote.model;

import java.util.List;

public class VoteService {

    private final String name;
    public String url;
    public int delay;
    public List<Reward> reward;

    public VoteService(String name) {
        this.name = name;
    }

    public String getService() {
        return name;
    }

}
