package online.greencore.litevote.model.commands;

import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VotifierEvent;
import net.md_5.bungee.api.ChatColor;
import online.greencore.litevote.LiteVote;
import online.greencore.litevote.model.User;
import online.greencore.litevote.model.UserHandler;
import online.greencore.litevote.model.VoteManager;
import online.greencore.litevote.model.VoteService;
import online.greencore.litevote.util.Permissions;
import online.greencore.litevote.util.PlaceholderFilter;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.Optional;

public class VoteCommands implements CommandExecutor {

    private final LiteVote plugin;
    private final UserHandler userHandler;
    private final VoteManager voteManager;

    public VoteCommands(LiteVote plugin) {
        this.plugin = plugin;
        this.userHandler = plugin.getUserHandler();
        this.voteManager = plugin.getVoteManager();
    }

    private void sendMessage(Player player, String message) {
        PlaceholderFilter.sendMessage(player, message);
    }

    private void sendDebugMessage(CommandSender sender, String message) {
        sender.sendMessage(ChatColor.GRAY + "(" + ChatColor.YELLOW + "" + ChatColor.BOLD + "!" + ChatColor.GRAY + ") " + message);
    }

    private boolean playerCommands(Player player, String[] args) {
        if (!Permissions.hasPermission(player, Permissions.VOTE)) {
            plugin.getMessages().invalidPermission.forEach(player::sendMessage);
            return true;
        }

        if (!(args.length >= 1)) {
            plugin.getMessages().voteLinks.forEach(message -> sendMessage(player, message));
            voteManager.getServices().forEach(((siteName, voteService) -> sendMessage(player, voteService.url)));
            return true;
        }

        final User user = userHandler.getUser(player.getUniqueId().toString());

        String command = args[0];
        switch (command) {
            case "check":
                if (plugin.getUserHandler().hasPendingClaims(player, user, false)) {
                    return true;
                }

                // if there's a username in the command args
                if (args.length >= 2) {
                    String name = args[1];
                    String uuid = LiteVote.getUUID(name);
                    Optional<Player> target = Optional.ofNullable(Bukkit.getPlayer(uuid));
                    if (target.isPresent()) {//target is online
                        plugin.getPlaceholders().other = target;
                    } else {
                        sendDebugMessage(player, "Not online");
                        return true;
                    }
//                    else { //offline lookup
//                        if (plugin.getStorage() instanceof JdbcStorage) {
////                        TODO offline lookup
//                        }
//                        sendDebugMessage(player, "Searching for " + uuid.get());
//                        return true;
//                    }
                }
                plugin.getMessages().claimStats.forEach(message -> sendMessage(player, message));
                return true;
            case "-c":
            case "claim":
                userHandler.hasPendingClaims(player, user, true);
                return true;
            case "help":
                plugin.getMessages().commandHelp.forEach(message -> sendMessage(player, message));
                if (Permissions.hasPermission(player, Permissions.ADMIN)) {
                    plugin.getMessages().commandAdminHelp.forEach(message -> sendMessage(player, message));
                }
                return true;
        }

        return debugCommand(player, args);
    }

    private boolean debugCommand(CommandSender sender, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (!Permissions.hasPermission(player, Permissions.ADMIN)) {
                plugin.getLogger().config(String.format("[%s] has no permission", player.getName()));
                return true;
            }
        }
        if (args.length >= 1 && args[0].equalsIgnoreCase("admin")) {
            if (args.length >= 2) {
                switch (args[1]) {
                    case "test":
                        if (args.length == 4) {
                            String name = args[2];
                            String service = args[3];
                            Vote vote = new Vote();
                            vote.setUsername(name);
                            vote.setServiceName(service);
                            Bukkit.getPluginManager().callEvent(new VotifierEvent(vote));
                        } else {
                            sendDebugMessage(sender, "Syntax: /lv admin vote <player> <service>");
                            sendDebugMessage(sender, "Available Services:");
                            voteManager.getServices().forEach((site, voteService) -> sendDebugMessage(sender, " - " + site));
                        }
                        return true;
                    case "reward":
                        if (args.length == 4) {
                            String name = args[2];
                            String service = args[3];
                            Optional<VoteService> vs = Optional.ofNullable(voteManager.getService(service));
                            if (vs.isPresent()) {
                                VoteService site = vs.get();
                                plugin.getRewardHandler().applyRewards(Bukkit.getPlayer(name), site.reward);
                            } else {
                                sendDebugMessage(sender, "Service: " + service + " doesn't exist");
                            }
                        } else {
                            sendDebugMessage(sender, "Syntax: /lv admin reward <player> <service>");
                        }
                        return true;
                    case "set":
                        if (args.length == 5) {
                            String name = args[2];
                            String UUID = LiteVote.getUUID(name);
                            User user = userHandler.getUser(UUID);
                            User oldUser = user;

                            String flag = args[3];
                            if (flag == null) {
                                sendDebugMessage(sender, "A flag must be defined");
                                sendDebugMessage(sender, " * -p Points");
                                sendDebugMessage(sender, " * -m Milestones");
                                return true;
                            }

                            int amount = Integer.parseInt(args[4]);
                            if (amount == 0) {
                                sendDebugMessage(sender, "Invalid amount");
                                return true;
                            }

                            switch (flag) {
                                case "-p":
                                    user.points = amount;
                                    break;
                                case "-m":
                                    user.milestone = amount;
                                    break;
                                case "-r":
                                    user = new User(UUID);
                                    flag = "RESET";
                                    break;
                                default:
                                    sendDebugMessage(sender, "Invalid flag: <-p Points | -m Milestones>");
                                    return true;
                            }
                            plugin.getStorage().save(user);

                            plugin.getLogger().config("(!) User Modified [flag=" + flag + "]\n[OLD]: " + oldUser.toString() + "\n[NEW]: " + user.toString());
                            sendDebugMessage(sender, "User Modified [" + name + ", " + flag + ", " + amount + "]");
                        } else {
                            sendDebugMessage(sender, "Syntax: ");
                            sendDebugMessage(sender, " /lv admin set <player> <flag | -p points | -m milestones> <amount>");
                        }
                        return true;
                }
            }
        }
        return true;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof ConsoleCommandSender) {
            ConsoleCommandSender console = (ConsoleCommandSender) sender;
            return debugCommand(console, args);
        }
        if (sender instanceof Player) {
            Player player = (Player) sender;
            return playerCommands(player, args);
        }
        return true;
    }
}
