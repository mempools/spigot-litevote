package online.greencore.litevote.model.listeners;

import online.greencore.litevote.LiteVote;
import online.greencore.litevote.model.UserHandler;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListeners implements Listener {

    private final LiteVote plugin;
    private final UserHandler userHandler;
    private String UUID;

    public PlayerListeners(LiteVote plugin) {
        this.plugin = plugin;
        this.userHandler = plugin.getUserHandler();
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onJoin(PlayerJoinEvent event) {
        UUID = event.getPlayer().getUniqueId().toString();
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> userHandler.hasPendingClaims(event.getPlayer(), userHandler.getUser(UUID), false));
//
//        if (!userHandler.getUserMap().containsKey(UUID)) {
//            Optional<User> check = plugin.getStorage().load(UUID);
//            userHandler.getUserMap().putIfAbsent(UUID, check.orElse(new User(UUID)));
//        }
//
//
//        if (!userHandler.getUserMap().containsKey(UUID)) { // if player isn't online
//            Optional<User> user = plugin.getStorage().load(UUID);
//            userHandler.getUserMap().put(UUID, user.orElse(new User(UUID)));
//        } else {
//            //returning players
//            User user = userHandler.getUser(UUID);
//            userHandler.hasPendingClaims(event.getPlayer(), user);
//        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onQuit(PlayerQuitEvent event) {
        UUID = event.getPlayer().getUniqueId().toString();
        userHandler.save(userHandler.getUser(UUID));
    }
}
