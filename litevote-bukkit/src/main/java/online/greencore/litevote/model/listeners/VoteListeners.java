package online.greencore.litevote.model.listeners;

import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VotifierEvent;
import online.greencore.litevote.LiteVote;
import online.greencore.litevote.model.*;
import online.greencore.litevote.util.Task;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Optional;

public class VoteListeners implements Listener {

    private final LiteVote plugin;
    private final UserHandler userHandler;
    private final VoteManager voteManager;
    private final MilestoneHandler milestoneHandler;
    private final RewardHandler rewardHandler;
    private final ArrayDeque<Task> tasks = new ArrayDeque<>();

    public VoteListeners(LiteVote plugin) {
        this.plugin = plugin;
        this.userHandler = plugin.getUserHandler();
        this.voteManager = plugin.getVoteManager();
        this.milestoneHandler = plugin.getMilestoneHandler();
        this.rewardHandler = plugin.getRewardHandler();
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onVoteEvent(VotifierEvent event) {
        tasks.add(() -> {
            Vote vote = event.getVote();
            plugin.getLogger().info(vote.toString());
            if (vote.getUsername().isEmpty()) return;
            Optional<VoteService> service = Optional.ofNullable(voteManager.getService(vote.getServiceName()));
            if (!service.isPresent()) {
                plugin.getLogger().info("Generating Unknown: " + vote.toString());
                voteManager.getServices().putIfAbsent(vote.getServiceName(), generate(vote));
            }

            String UUID = LiteVote.getUUID(vote.getUsername());
            User user = userHandler.getUser(UUID);
            user.online = Bukkit.getPlayer(vote.getUsername()) != null;
            user.addTotals(vote.getServiceName(), 1);
            milestoneHandler.addMilestone(user, 1);
            user.addPoints(1);

            if (user.online) {
                Player player = Bukkit.getPlayer(vote.getUsername());
                service.ifPresent(s -> rewardHandler.applyRewards(player, service.get().reward));
                milestoneHandler.claimMilestoneRewards(player, user);
            } else {
                user.offlineVotes.add(vote.getServiceName());
            }
            Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> plugin.getUserHandler().save(user), 50L);
        });

        tasks.forEach(Task::execute);
        tasks.removeLast();
    }

    private VoteService generate(Vote vote) {
        Optional<String> generate = Optional.ofNullable(plugin.getProperties().getProperty("generate_sites", "true"));
        generate.ifPresent(enabled -> {
            if (!enabled.contains("true")) return;
            Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
                File gen = new File(plugin.getDataFolder() + File.separator + "sites", vote.getServiceName() + ".yml");
                try {
                    if (!gen.exists() && gen.createNewFile()) {
                        FileConfiguration config = plugin.getConfig();
                        config.addDefault("enabled", false);
                        config.addDefault("delay", 24);
                        config.addDefault("service", vote.getServiceName());
                        config.addDefault("url", vote.getAddress());
                        config.addDefault("rewards", Collections.singletonList("BasicReward"));
                        config.options().copyDefaults(true);
                        config.save(gen);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                plugin.getLogger().info("Generating configuration file: " + vote.getServiceName());
            });
        });
        return new VoteService(vote.getServiceName());
    }
}

