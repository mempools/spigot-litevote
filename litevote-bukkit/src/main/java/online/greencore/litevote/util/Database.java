package online.greencore.litevote.util;

import com.zaxxer.hikari.HikariDataSource;
import online.greencore.litevote.LiteVote;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;

public class Database {

    private final Logger logger = Logger.getLogger(Database.class.getName());
    private final LiteVote plugin;
    private final HikariDataSource dataSource = new HikariDataSource();
    private Connection connection;

    public Database(LiteVote plugin) {
        this.plugin = plugin;
    }

    private void createConnection() {
        dataSource.setAutoCommit(false);
        dataSource.setJdbcUrl(plugin.getProperties().getProperty("mysql_url"));
        dataSource.setUsername(plugin.getProperties().getProperty("mysql_user"));
        dataSource.setPassword(plugin.getProperties().getProperty("mysql_pass"));
        try {
            connection = dataSource.getConnection();
        } catch (SQLException e) {
            logger.severe("MySQL Error" + e.getMessage());
        }
    }

    public Connection getConnection() {
        if (connection == null) {
            createConnection();
        }
        return connection;
    }

}
