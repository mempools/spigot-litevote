package online.greencore.litevote.util;

import online.greencore.litevote.LiteVote;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class IOUtils {

    private static void copy(InputStream source, File dest) {
        try {
            try (InputStream input = source; OutputStream output = new FileOutputStream(dest)) {
                byte[] buf = new byte[1024];
                int bytesRead;
                while ((bytesRead = input.read(buf)) > 0) {
                    output.write(buf, 0, bytesRead);
                }
                input.close();
                output.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadResource(LiteVote plugin, String file, String path) {
        File folder = new File(plugin.getDataFolder(), path);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        File resource = new File(folder, file);
        try {
            if (!resource.exists()) {
                resource.createNewFile();
                IOUtils.copy(plugin.getResource(file), resource);
                plugin.getLogger().config("Created file: " + resource.getName() + " @" + resource.getPath());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void checkResource(LiteVote plugin, String file, String path) {
        File check = new File(new File(plugin.getDataFolder(), path), file);
        if (!check.exists()) {
            loadResource(plugin, file, path);
        }
    }
}
