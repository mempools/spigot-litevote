package online.greencore.litevote.util;

import me.clip.placeholderapi.external.EZPlaceholderHook;
import online.greencore.litevote.LiteVote;
import online.greencore.litevote.model.User;
import org.bukkit.entity.Player;

import java.util.Optional;

public class LiteVotePlaceholders extends EZPlaceholderHook {

    public Optional<Player> other = Optional.empty();
    private LiteVote plugin;

    public LiteVotePlaceholders(LiteVote plugin) {
        super(plugin, "litevote");
        this.plugin = plugin;
    }

    @Override
    public String onPlaceholderRequest(Player player, String identifier) {
        player = other.orElse(player);
        if (player == null) return "";
        switch (identifier) {
            case "player":
                return player.getName();
            case "player_display":
                return player.getDisplayName();
        }

        User user = plugin.getUserHandler().getUser(player.getUniqueId().toString());
        if (user == null) return "";

        switch (identifier) {
            case "points":
                return String.valueOf(user.points);
            case "milestones":
                return String.valueOf(user.milestone);
            case "totals":
                return String.valueOf(user.getTotal());
            case "unclaimed_votes":
                return String.valueOf(user.offlineVotes.size());

        }
        return null;
    }
}
