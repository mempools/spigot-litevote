package online.greencore.litevote.util;

import online.greencore.litevote.LiteVote;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MessagesConfiguration {

    public List<String> invalidPermission = new ArrayList<>();
    public List<String> claimNothing = new ArrayList<>();
    public List<String> unclaimedRewards = new ArrayList<>();
    public List<String> unclaimedMilestones = new ArrayList<>();
    public List<String> commandHelp = new ArrayList<>();
    public List<String> commandAdminHelp = new ArrayList<>();
    public List<String> claimStats = new ArrayList<>();
    public List<String> voteLinks = new ArrayList<>();

    public MessagesConfiguration(LiteVote plugin) {
        File messagesFile = new File(plugin.getDataFolder() + File.separator + "messages.yml");
        YamlConfiguration parser = YamlConfiguration.loadConfiguration(messagesFile);
        this.invalidPermission = parser.getStringList("invalid-permission");
        this.claimNothing = parser.getStringList("nothing-claimed");
        this.unclaimedRewards = parser.getStringList("unclaimed-rewards");
        this.unclaimedMilestones = parser.getStringList("unclaimed-milestones");
        this.claimStats = parser.getStringList("claim-statistics");
        this.commandHelp = parser.getStringList("command-help");
        this.commandAdminHelp = parser.getStringList("command-admin-help");
        this.voteLinks = parser.getStringList("vote-links");
    }

}
