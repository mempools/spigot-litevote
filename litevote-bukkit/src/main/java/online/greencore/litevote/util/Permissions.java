package online.greencore.litevote.util;

import org.bukkit.entity.Player;

public class Permissions {

    public static final String VOTE = "litevote.vote", ADMIN = "litevote.admin";

    private static boolean hasPermission(Player player, String permission, boolean error) {
        if (!player.hasPermission(permission) && error) {
            player.sendMessage("You do not have permission!");
        }
        return player.hasPermission(permission);
    }

    public static boolean hasPermission(Player player, String permission) {
        return hasPermission(player, permission, false);
    }
}
