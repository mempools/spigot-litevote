package online.greencore.litevote.util;

import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.entity.Player;

public class PlaceholderFilter {

    public static String filter(Player player, String text) {
        return PlaceholderAPI.setPlaceholders(player, text);
    }

    public static void sendMessage(Player player, String message) {
        player.sendMessage(PlaceholderFilter.filter(player, message));
    }
}
