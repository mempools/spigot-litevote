package online.greencore.litevote.util;

public interface Task {

    void execute();
}
